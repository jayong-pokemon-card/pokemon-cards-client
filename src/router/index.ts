import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Home from '../views/Home.vue';
import Login from '../components/Login.vue';
import NotFound from '../views/NotFound.vue';


Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
    {
        path: "/",
        redirect: "/home"
    },
    {
        path: '/home',
        name: 'Home',
        component: Home,
        meta: { requiresAuth: true }
    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
    },
    {
        path: '*',
        name: 'Not Found',
        component: NotFound
    }
];

const router = new VueRouter({
    mode: 'history',
    routes
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        const isLoggedOn = localStorage.getItem('isLoggedOn');
        if (!isLoggedOn) {
            next({
              path: '/login',
              query: { redirect: to.fullPath }
            });
        } else {
            next();
        }
    } else {
        next() ;
    }
});

export default router
